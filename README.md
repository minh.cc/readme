# Hướng dẫn cài đặt và chạy dự án

## Cài đặt Node.js

1. **Node.js**: Nest.js yêu cầu Node.js, một runtime environment cho JavaScript.
   - Tải xuống và cài đặt Node.js từ trang chủ: [nodejs.org](https://nodejs.org/)
   - Phiên bản khuyến nghị: LTS (Long-Term Support)
   - Node version: v18.20.2 (LTS)

2. **Kiểm tra cài đặt**:
   - Mở terminal (hoặc command prompt trên Windows).
   - Chạy lệnh sau để kiểm tra phiên bản Node.js:
     ```
     node -v
     ```
   - Chạy lệnh sau để kiểm tra phiên bản npm (Node Package Manager):
     ```
     npm -v
     ```

## Cài đặt Yarn

1. **Yarn CLI**: Công cụ này giúp tạo và quản lý dự án Nest.js một cách dễ dàng.
   - Cài đặt Yarn CLI toàn cục bằng npm:
     ```
     npm install -g yarn
     ```

   - Chạy lệnh sau để kiểm tra phiên bản npm (Node Package Manager):
     ```
     yarn -v
     ```

## Chạy dự án

### Server

1. **Clone dự án**:
   - Clone dự án từ repository hoặc tải xuống từ một nguồn nào đó.
   - Thư mục: p36traficlight-backend

2. **Cài đặt các dependencies**:
   - Di chuyển vào thư mục dự án: 
     ```
     cd p36traficlight-backend
     ```
   - Chạy lệnh sau để cài đặt các dependencies:
     ```
     yarn install
     ```

3. **Chạy dự án**:
   - Sau khi cài đặt xong, bạn có thể chạy dự án bằng lệnh:
     ```
     yarn start
     ```
   - Hoặc nếu bạn muốn khởi động lại dự án khi có sự thay đổi trong mã nguồn, sử dụng:
     ```
     yarn start:dev
     ```

4. **Truy cập ứng dụng**:
   - Mở trình duyệt và truy cập vào địa chỉ sau:
     ```
     http://localhost:4036/api
     ```


### FrontEnd

1. **Clone dự án**:
   - Clone dự án từ repository hoặc tải xuống từ một nguồn nào đó.
   - Thư mục: p36traficlight-admin

2. **Cài đặt các dependencies**:
   - Di chuyển vào thư mục dự án: 
     ```
     cd p36traficlight-admin
     ```
   - Chạy lệnh sau để cài đặt các dependencies:
     ```
     yarn install
     ```

3. **Chạy dự án**:
   - Sau khi cài đặt xong, bạn có thể chạy dự án bằng lệnh:
     ```
     yarn start
     ```


4. **Truy cập ứng dụng**:
   - Mở trình duyệt và truy cập vào địa chỉ sau:
     ```
     http://localhost:4200
     ```


## Câu hỏi và hỗ trợ

Nếu bạn gặp vấn đề trong quá trình cài đặt hoặc chạy dự án, hãy liên hệ qua email hoặc tạo một issue trên repository của dự án.